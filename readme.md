Sea worms are a large and snake-like monster that resides in the sea or in lakes. The creature is usually 10 - 12 meters long, black or gray and shiny. Some, however, say that they have seen sea worms up to 30 - 40 meters. People who have observed sea worms often describe that they have heads that look like crocodiles, horses or moose. They migrate from small ponds into the sea according to lore,- and sometimes gets stuck in waterfalls, dies and causes a horrendous stench that brings disease. Also, the large ones that live in saltwater can attack sailing ships and eat people.

Sea worms have been observed in over 50 Norwegian lakes and ponds.

Ever since the 15th century, detailed descriptions of sea worms in Norwegian fjords have been given, often accompanied by dramatic descriptions of how sea worms looked and behaved. In the 19th century, however, there were significantly higher numbers of sightings of sea worms in the sea and in Norwegian fjords than in the 20th century.

According to the descriptions, the sea worms along the Norwegian coast and Norwegian fjords are largely identical to the sea worms in Norwegian lakes, just larger. They are referred to as huge, black-brown snake-like sea creatures with humps. The head is described as large, pointed or rounded.

In 1969, two brothers were on a fishing trip in Hallandsvannet by Mandal in Vest-Agder. They suddenly came across a sea serpent, and one man threw a large stone at the creature's head. The brothers described the worm as long and black with a red mouth and large, round eyes. On the head it had sensory horns, and the nostrils resembled a newborn foal.

In Sogn og Fjordane, sea worms have been observed in Hornindalsvatnet,Loenvatnet and Jølstervatnet.

There have been several observations in Sandnesvatnet in Hamarøy (Nordland). The creature had a body reminiscent of a capsized barge, and the sea worm's head resembled a small horse.

In Åfjordvatnet (Finnmark) west of Hammerfest, a sea serpent was seen in 1977. There were 4 people who observed the strange creature, and it was described as 5 meters long with several humps.

Lake Kørelen on Sotra iceland outside Bergen is famous for its sea serpent "Kørelatrollet". According to the parish books, the sea worm was last seen in 1910. Parish priest Johan Fritzner Greve (1832 - 1907) himself is said to have been chased by "Kørelatrollet", and he also told the story that the sea worm overturned the boat to a bridal party, and then swallowed all the people.

Seljord is famous for its sea serpent, Selma, who allegedly lives in Lake Seljord ( Seljordsvatnet ).

Places in Norwegian fjords where sea worms have been observed:

Romsdalsfjorden is located in Møre og Romsdal county, and a number of people have throughout history observed sea worms here. One summer day in 1815, 4 men were on a fishing trip, when they suddenly discovered a large sea animal. It moved slowly, curved like a worm, and had 10 fins on the front near its head.

The sea worm was described as thick, dark and up to 50 feet long (about 15 meters). The head was huge with a pointed snout. As the sea worm approached the boat, one of the men shot the worm in the head. It dived under water but immediately came up again. Then it swam quickly after the boat. The men steered the boat ashore, and when they reached narrower waters, the sea serpent appeared under water again. Then it disappeared into the depths.The sea worm in Romsdalsfjorden was most frequently observed in the middle of the 19th century.

In the Hessafjord near Ålesund, a sea worm approx. 200 meters from land. It was up to 30 meters long, 1.5 meters thick and had a huge square fin. The spotter ran home to get his camcorder to film the creature. The sea serpent was still there when he returned, and was working on a whale carcass. Unfortunately, the video recording did not did not show any clear details of the sea worm, as the distance was too far.

In Østfold county, sea worms have been observed on Hvaler in 1902. Parish priest Hans Davidsen was sailing with 6 other adults when they spotted the sea animal. It was described as "an unknown sea creature with three dark and glistening humps". The creature was 15 - 20 meters long and had a head that swung from side to side. The parish priest noted the incident in the parish book for Hvaler, and attached a sketch of the sea serpent.

Seaworms have been seen in both Aust- and Vest-Agder. One was observed at Grimstad in 1980, and the other was seen at Søgne in 1982. Both sea creatures were described as dark with 3 humps.

Both by Ølensundet in Boknafjorden and outside Karmøy in Rogaland county, there are old stories about people who have seen sea worms. At Karmøy, the observation took place in the 1830s.

Sea worms have also been observed in Finnmark, and in 1894 two pieces were noticed in Ersvika south of Hammerfest. The sea worms stayed for 2 months in the same place, and were seen by many people.

The curious case of Mjøsormen:

The first time Mjøsormen is mentioned in a written source is in 1515. Then Archbishop Erik Akselssøn Valkendorf in Nidaros ( now Trondheim) wrote to Pope Leo X that there was a large sea worm in Mjøsa.

Archbishop Erik Akselssøn Valkendorf described the large sea worms as gray in color and dangerous to humans. It only appeared when the sea was calm, and if anyone was to encounter it while out in a boat, they had to steer straight into the eyes of the worm to have any chance of survival.

A special and very concrete episode in a written report from 1522, reproduced in "Hamarkrøniken" ( Hamar cronicles), tells that a large sea serpent was sighted in Lake Mjøsa, and got stuck on a cliff near the the land that protrudes out in lake Mjøsa not far away from Hamar Cathedral.

Its body had many colors and it had a mane like a horse. It head was shaped like a horses head, and it had a mane like a horse along its neck. It was shot many times in the eyes with a crossbow by one of the bishop's brave servants, drifted to Helgøya island and started to rot and smelled so bad that the people complained to Bishop Mogens Lauritssøn who was the local bishop. He then commanded people to gather wood and burn the corpse.

The spine remained on the beach of Helgøya for many years, and some of the vertebrae was used as blocks to chop wood. They was so heavy that two strong men struggled to lift one.

The ruins of the Cathedral still exists in Hamar and can be found on maps as Hamardomen.

In reality, the description of a swimming sea worms head fits quite well with a swimming moose as these are good swimmers and have no issues with swimming across Mjøsa.
